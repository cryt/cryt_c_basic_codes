#include "header.h"

void change_name(struct student * p, char * pchar)
{
	int i;
	for (i = 0; pchar[i] != '\0' && i < MAX_NAME_LEN; ++i)
	{
		p->name[i] = pchar[i];
	}
}