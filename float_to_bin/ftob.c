#include <stdio.h>

int main(void)
{
	float fnum = 1;
	int i;
	int * p = (int *)&fnum;
	while (fnum != 0)
	{
		printf("input a num: ");
		scanf("%f", &fnum);
		for (i = 31; i >= 0; i--)
		{
			printf("%d", (*p & (1 << i) ? 1: 0));
		}
		printf("\n");
	}
	return 0;
}