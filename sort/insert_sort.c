#include <stdio.h>

#define ARR_LEN 20

void insert_sort(int * p, int n);

int main(void)
{
	int n;
	int a[ARR_LEN];
	int i;
	scanf("%d", &n);
	for (i = 0; i < n; ++i)
	{
		scanf("%d", a+i);
	}
	
	insert_sort(a, n);

	for (i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");

	return 0;
}

void insert_sort(int * a, int n)
{
	int i, j;
	int tmp;
	for (i = 1; i < n; ++i)
	{
		tmp = a[i];
		j = i - 1;

		while (j >= 0 && a[j] > tmp)
		{
			a[j+1] = a[j];
			--j;
		}
		a[j+1] = tmp;
	}
}