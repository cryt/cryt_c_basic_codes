#ifndef _my_list_H_

#include <stdio.h>
#include <malloc.h>

#define TRUE 1
#define FALSE 0
#define BOOL int
#define OVERFLOW -1

struct listnode
{
	void * pelem;
	struct listnode * next;
};
struct list
{
	int elemsize;	//the size of *pelem
	struct listnode headnode;	
};

struct list * create_list(int elemsize);
BOOL make_list_empty(struct list * pL);
BOOL is_list_empty(const struct list * pL);
BOOL is_listelem_last(const struct list * pL, const struct listnode * pnode);
struct listnode * find_listelem(struct list * pL, void * x, BOOL my_equal());
BOOL delete_listelem(struct list * pL, void * x, BOOL my_equal());
struct listnode * find_previous_listnode(struct list * pL, void * x);
struct listnode * find_next_listnode(struct list * pL, void * x);
BOOL insert_listelem(struct list * pL, struct listnode * posi, void * x);	//insert after posi

#endif