#include <stdio.h>

#define NUM_MAX_LENGTH 30

/*输入参数：res指向存放结果的缓冲区 
* 			如果结果是12 则res[0]=1 res[1]=2 res[2]='\0'
*			n 转换之前的数字 int类型 十进制形式
*			base 转换基数 2表示转换为2进制 16表示转换为16进制
* 返回值： 无
*/
void number_base_convert(char * res, int n, int base);

int main(void)
{
	int n;
	int base;
	int i;
	char result[NUM_MAX_LENGTH];
	printf("input a number: ");
	scanf("%d", &n);

	printf("input digit base: ");
	scanf("%d", &base);

	number_base_convert(result, n, base);

	printf("%d base digit: %s\n", base, result);

	return 0;
}

void number_base_convert(char * res, int n, int base)
{
	static char digit[] = "0123456789ABCDEF";
	char char_stack[NUM_MAX_LENGTH];
	int top = -1;
	int i = -1;
	while (n > 0)
	{
		++top;
		char_stack[top] = digit[n % base];

		n /= base;
	}
	while (top >= 0)
	{
		++i;
		res[i] = char_stack[top];
		--top;
	}
	res[i+1] = '\0';

}
