#include <stdio.h>

#include "my_list.h"
#include "my_list.c"

void showmenu(void);

int main(void)
{
	int op;
	struct list * pL;
	showmenu();
	printf("please input a num between 1 to 9\n");
	scanf("%d", &op);
	while (1)
	{
		switch (op)
		{
			case 1:
				if ((pL = create_list(sizeof(int))) != NULL)
					printf("ok\n");
				break;
			default:
				break;
		}
	}	

	return 0;
}


void showmenu(void)
{
	printf("1. creat_list\n");
	printf("2. make_list_empty\n");
	printf("3. is_list_empty\n");
	printf("4. is_listelem_last\n");
	printf("5. find_listelem\n");
	printf("6. delete_listelem\n");
	printf("7. find_previous_listnode\n");
	printf("8. find_next_listnode\n");
	printf("9. insert_listelem\n");
}