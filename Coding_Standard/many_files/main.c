#include "header.h"

#include "change_id.h"
#include "change_name.h"

int main(void)
{
	int op = 1;
	int id;
	char name[MAX_NAME_LEN];
	struct student zhangsan = {1, "lisi"};

	while (op != 0)
	{
		printf("0. exit\n");
		printf("1. change_id\n");
		printf("2. change_name\n");
		scanf("%d", &op);
		switch(op)
		{
			case 1:
			{
				printf("input id: ");
				scanf("%d", &id);
				if (change_id(&zhangsan, id) == TRUE)
					;
				else
					printf("id is not corrext\n");

				break;
			}
			case 2:
			{
				printf("input name\n");
				scanf("%s", name);
				change_name(&zhangsan, name);

				break;
			}
			default:
			{
				break;
			}
		}

		printf("now zhangsan\'s id is %d.\n", zhangsan.id);
		printf("and zhangsan\'s name is %s.\n", zhangsan.name);
	}


	return 0;
}