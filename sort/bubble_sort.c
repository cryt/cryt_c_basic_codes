#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1000

void bubble_sort1(int *a, int low, int high);

int main(void)
{
	int i;
	int a[N];

	srand((int)time(NULL));

	for (i = 0; i < N; ++i)
	{
		a[i] = rand() % 10000;
	}

	bubble_sort1(a, 0, N);

	for (i = 0; i < N; ++i)
		printf("%d ", a[i]);
	printf("\n");

	return 0;
}

void bubble_sort1(int *a, int low, int high)
{
	int last = high - 1;
	int tmp;
	int sorted;
	int i;
	while (last > low)
	{
		i = low;
		sorted = low;
		while (i < last)
		{
			if (a[i] > a[i+1])
			{
				tmp = a[i+1];
				a[i+1] = a[i];
				a[i] = tmp;
				sorted = i+1;
			}
			++i;
		}
		last = sorted;
	}
}