#include <stdio.h>

long power(int a, int b);	//

int main(void)
{
	int a, b;
	while (1)
	{
		printf("please input two numbers\n");
		scanf("%d %d", &a, &b);

		printf("pow %d of %d is %ld\n",a, b, power(a, b));
	}
	return 0;
}

long power(int a, int b)
{
	if (b == 0)
		return 1;
	if (b % 2 == 0)
		return power(a * a, b / 2);
	if (b % 2)
		return power(a * a, b / 2) * a;

}