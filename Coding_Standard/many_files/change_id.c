#include "header.h"

BOOL check_id (int id)
{
	if (id > 100)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

BOOL change_id(struct student * p, int id)
{
	if (check_id(id) == FALSE)
	{
		return FALSE;
	}
	else
	{
		p->id = id;
		return TRUE;
	}
}
