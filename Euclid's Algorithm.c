#include <stdio.h>

int gcd(int a, int b);	//Greatest Common Divisor

int main(void)
{
	int a, b;
	while (1)
	{
		printf("please input two numbers\n");
		scanf("%d %d", &a, &b);

		printf("the Greatest Common Divisor of %d and %d is %d\n",a, b, gcd(a, b));
	}
	return 0;
}

int gcd(int a, int b)
{
	int x;
	while (b > 0)
	{
		x = a % b;
		a = b;
		b = x;
	}
	return a;
}