#include <stdio.h>

#define BOOL int
#define TRUE 1
#define FALSE 0

#define MAX_NAME_LEN 30

struct student
{
	int id;
	char name[MAX_NAME_LEN];
};