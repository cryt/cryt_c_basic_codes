#include <stdio.h>

int binarysearch1(int a[], int x, int low, int high);	
int binarysearch2(int a[], int x, int low, int high);	
int binarysearch3(int a[], int x, int low, int high);	

int main(void)
{
	int a[12] = {1,2,5,7,9,11,12,23,45,55,78,100};
	printf("%d\n", binarysearch1(a, 1, 0, 12));

	return 0;
}

int binarysearch1(int a[], int x, int low, int high)
{
	int mid;
	while (low < high)
	{
		mid = (low + high) / 2;
		if (x < a[mid])
			high = mid;
		else if (a[mid] < x)
			low = mid + 1;
		else
			return mid;
	}
	return -1;
}
int binarysearch2(int a[], int x, int low, int high)
{
	int mid;
	while (high - low > 1)
	{
		mid = (low + high) / 2;
		x < a[mid] ? high = mid : low = mid;
	}
	return (x == a[low]) ? low : -1;
}
int binarysearch3(int a[], int x, int low, int high)
{
	int mid;
	while (low < high)
	{
		mid = (low + high) / 2;
		x < a[mid] ? high = mid : low = mid + 1;
	}
	return --low;
}