struct list * create_list(int elemsize)
{
	struct list * pL;
	if ((pL = malloc(sizeof(struct list))) == NULL)
		exit(OVERFLOW);
	pL->elemsize = elemsize;
	pL->headnode.pelem = NULL;
	pL->headnode.next = NULL;

	return pL;
}
BOOL make_list_empty(struct list * pL);
BOOL is_list_empty(const struct list * pL);
BOOL is_listelem_last(const struct list * pL, const struct listnode * pnode);
struct listnode * find_listelem(struct list * pL, void * x, BOOL my_equal());
BOOL delete_listelem(struct list * pL, void * x, BOOL my_equal());
struct listnode * find_previous_listnode(struct list * pL, void * x);
struct listnode * find_next_listnode(struct list * pL, void * x);
BOOL insert_listelem(struct list * pL, struct listnode * posi, void * x);	//insert after posi