#include <stdio.h>

int main(int argc, char * argv[])
{
	FILE * fp;
	int ch;
	printf("there are %d arguments.\n", argc);
	printf("argc = %d\n", argc);
	printf("argv[0] = %s\n", argv[0]);
	printf("argv[1] = %s\n", argv[1]);

	if ((fp = fopen(argv[1], "r")) == NULL)
		printf("fail to open %s\n", argv[1]);

	printf("text content: \n");
	while ((ch = fgetc(fp)) != EOF)
	{
		putchar(ch);
	}

	printf("\nPress Enter to Exit...\n");
	getchar();
	

	return 0;
}